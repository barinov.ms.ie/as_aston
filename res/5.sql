CREATE TABLE marks (
    id SERIAL PRIMARY KEY,
    student_id INTEGER references students(id) NOT NULL,
    subject_id INTEGER references subject(id) NOT NULL,
    value INTEGER CHECK (value > 0 & value =< 5),
    publication_date DATE CHECK (publication_date <= CURRENT_DATE) DEFAULT CURRENT_DATE
);

CREATE TABLE students (
    id SERIAL PRIMARY KEY,
    group_id INTEGER references groups(id) NOT NULL,
    first_name varchar(255) NOT NULL,
    last_name  varchar(255) NOT NULL
);

CREATE TABLE subj_grup_prof (
    group_id INTEGER references groups(id) NOT NULL,
    subject_id INTEGER references subject(id) NOT NULL,
    professor_id INTEGER references professors(id) NOT NULL
);

CREATE TABLE professors (
    id SERIAL PRIMARY KEY,
    first_name varchar(255) NOT NULL,
    last_name  varchar(255) NOT NULL
);

CREATE TABLE groups (
    id SERIAL PRIMARY KEY
);

CREATE TABLE subjects (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL UNIQUE,
    about varchar(1000),
);